"""
    @author Yohann BENETREAULT
"""

import math
import matplotlib.pyplot as plt
import numpy as np

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

x = []
y = []
A = 10

def calculateRastrigin(xi, yi):
    """
        Calcul d'un bout de la somme
    """
    return (math.pow(xi,2) - A*math.cos(2*math.pi*xi)) + (math.pow(yi,2) - A*math.cos(2*math.pi*yi))

# === MAIN ===
if __name__ == "__main__":
    # Matrices
    x = np.arange(-5, 5, 0.10)
    y = np.arange(-5, 5, 0.10)
    # Matrice de résultats
    results = np.zeros((len(x),len(y)))

    # Calcul des résultats
    for i in range(0, len(x)):
        for j in range(0, len(y)):
            results[i][j] = A * 2 + calculateRastrigin(x[i], y[j])

    X, Y = np.meshgrid(x, y)

    figure = plt.figure()
    ax = Axes3D(figure)
    surf = ax.plot_surface(X, Y, results, rstride=1, cstride=1, cmap=cm.jet, linewidth=0, antialiased=False)

    plt.show()
# === FIN MAIN ===
